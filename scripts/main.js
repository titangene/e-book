const bookApi = './assets/book.json';

var bookContentElement = document.querySelector('.book-content');
var chapterElements;
var currentSectionOfChapterElements;
var prevPageButtonElement = document.querySelector('.prev-page');
var nextPageButtonElement = document.querySelector('.next-page');
var currentPageElement = document.querySelector('.current-page');
var tableOfContentContainerElement = document.querySelector('.table-of-content-container');
var tableOfContentButtonElement = document.querySelector('.table-of-content-btn');
var tableOfContentElement = document.querySelector('.table-of-content');
var tableOfContentBGElement = document.querySelector('.table-of-content-bg');
var tableOfContentBackElement = document.querySelector('.table-of-content-back');
var backContainerElement = document.querySelector('.back__container');
var featureBarElement = document.querySelector('.featurebar__container');

// 因為有 "引言"，所以 currentChapter 預設為 0
// 因為要計算 section 切到哪一頁，所以 section 預設為 1，代表第一頁
var currentChapter = 0;
var currentSection = 1;
var currentSectionPage = 1;

var contentWidth;
var contentHeight;
var pageGap = {
  horizontal: 72,
  vertical: 89
};
var translatePosition = {
  horizontal: null,
  vertical: null
};

var fakePosition = {
  chapter: 0,
  section: 1,
  startIndex: 5,
  endIndex: 20,
  highlightText: '些人對他們所做的事情如此擅長，擅',
  color: '#c4beff',
  comment: 'test'
};

localStorage.setItem('text', JSON.stringify(fakePosition));


// e.g. 紀錄每個 chapter 的 section 總頁數
// { chapter: [ { section: [1, 5] }, { section: [2, 3] } ] }
var bookPageSum = {
  chapter: []
};

// 文直/文橫 切換 (debug 用)
var currentTextDirection = 'horizontal';

fetch(bookApi).then(function (response) {
  return response.json();
}).then(function(json) {
  console.log(json);
  tableOfContentElement.innerHTML = createTableOfContent(json);
  jsonToHtml(json);
  initBookContent();
  tableOfContentClickEvent();
  console.log(bookPageSum.chapter[currentChapter].section);
  console.log(`chapter:${currentChapter}/${getChapterSum()}, section:${currentSection}/${getCurrentSectionSum()}, page:${currentSectionPage}/${getCurrentSectionPageSum()}`);
});

function jsonToHtml(json) {
  var bookData = json.reverse();
  bookData.forEach((chapterData, chapterIndex) => {
    chapterElement = document.createElement("section");
    chapterElement.className = 'chapter';

    chapter = chapterData.chapter;
    chapterTitle = chapterData.title;
    chapterHtml = `<h2>${chapter} ${chapterTitle}</h2>`;
    chapterElement.innerHTML = chapterHtml;

    chapterSections = chapterData.sections;
    chapterSections.forEach((section, sectionIndex) => {
      var sectionContentElement = document.createElement("section");
      sectionContentElement.className = 'section-content';
      
      sectionTitleHtml = `<h3>${section.title}</h3>`;
      if (chapterIndex === bookData.length - 1 && sectionIndex === 0) {
        sectionContentHtml = `<p>${perTextIntoSpanElement(section.content)}</p>`;
      } else {
        sectionContentHtml = `<p>${section.content}</p>`;
      }
      
      sectionContentElement.innerHTML = sectionTitleHtml + sectionContentHtml;
      chapterElement.appendChild(sectionContentElement);
    });

    bookContentElement.insertAdjacentHTML('afterbegin', chapterElement.outerHTML);
  });
}

function perTextIntoSpanElement(sectionContent) {
  var temp = '';
  sectionContent.split('').forEach((text, index) => {
    var textIndex = index + 1;
    temp += `<span text-index="${textIndex}">${text}</span>`; 
  });
  return temp;
}

function initBookContent() {
  chapterElements = document.querySelectorAll('.chapter');
  updateCurrentSectionOfChapterElements();

  createNextPageOnClickEvent(currentTextDirection);
  createPrevPageOnClickEvent(currentTextDirection);

  var headerTwoOuterHeight = getHeaderTwoOuterHeight();
  contentWidth = chapterElements[0].clientWidth;
  // 內容高不包含 h2，因換頁不會變更 h2 的位置 
  contentHeight = chapterElements[0].clientHeight - headerTwoOuterHeight;

  translatePosition['horizontal'] = contentWidth + pageGap['horizontal'];
  translatePosition['vertical'] = contentHeight + pageGap['vertical'];

  initAllSectionPageSum(currentTextDirection);
  currentPageElement.innerText = 
    setCurrentPageText(currentSectionPage, getCurrentSectionPageSum());
  
  fakeHighlight();
}

function createTableOfContent(json) {
  var tableOfContentHtml = `<ul><li chapter="cover">封面</li>`;
  json.forEach((chapterData, chpaterIndex) => {
    if (chapterData.chapter)
      tableOfContentHtml += `
        <li>
          <span chapter="${chpaterIndex}">${chapterData.chapter} ${chapterData.title}</span>
          <ul>`;
    else
      tableOfContentHtml += `
        <li>
          <span chapter="${chpaterIndex}">${chapterData.chapter} ${chapterData.title}</span>
          <ul>`;

    chapterData.sections.forEach((section, sectionIndex) => {
      tableOfContentHtml += `
        <li chapter="${chpaterIndex}"
            section="${sectionIndex + 1}">${section.title}</li>`;
    });

    tableOfContentHtml += '</ul></li>';
  });

  tableOfContentHtml += '</ul>';
  return tableOfContentHtml;
}



function tableOfContentClickEvent() {
  tableOfContentButtonElement.addEventListener('click', () => {
    if (tableOfContentBGElement.classList.contains('display-none')) {
      tableOfContentBGElement.classList.remove('display-none');
      featureBarElement.classList.add('display-none');
      backContainerElement.classList.add('display-none');
    }
  });
  tableOfContentBGElement.addEventListener('click', e => {
    if ((e.target === tableOfContentBGElement ||
        e.target === tableOfContentBackElement) &&
        !tableOfContentBGElement.classList.contains('display-none')) {
      tableOfContentBGElement.classList.add('display-none');
      featureBarElement.classList.add('display-none');
      backContainerElement.classList.add('display-none');
    }
  });

  var tocUlElements = tableOfContentContainerElement.querySelectorAll('.table-of-content span');
  tocUlElements.forEach(liElement => {
    liElement.addEventListener('click', function() {
      console.log('chapter', this);

      hideCurrentSectionElement();
      hideCurrentChapterElement();
      currentChapter = this.getAttribute('chapter');
      currentSection = 1;
      currentSectionPage = 1;
      updateCurrentSectionOfChapterElements();
      showCurrentChapterElement();
      showCurrentSectionElement();
      tableOfContentBGElement.classList.add('display-none');

      console.log(`prev log - chapter:${currentChapter}/${getChapterSum()}, section:${currentSection}/${getCurrentSectionSum()}, page:${currentSectionPage}/${getCurrentSectionPageSum()}`);
    });

    var subLiElements = tableOfContentContainerElement.querySelectorAll('li li');

    subLiElements.forEach(subLiElement => {
      subLiElement.addEventListener('click', function () {
        console.log('sub li', this);
        
        hideCurrentSectionElement();
        hideCurrentChapterElement();
        currentChapter = this.getAttribute('chapter');
        currentSection = this.getAttribute('section');
        currentSectionPage = 1;
        updateCurrentSectionOfChapterElements();
        showCurrentChapterElement();
        showCurrentSectionElement();
        tableOfContentBGElement.classList.add('display-none');

        console.log(`prev log - chapter:${currentChapter}/${getChapterSum()}, section:${currentSection}/${getCurrentSectionSum()}, page:${currentSectionPage}/${getCurrentSectionPageSum()}`);
      });
    });
  });

  tableOfContentElement.addEventListener('click', () => {
    
  });
}

function hideAllBookContent() {
  chapterElements.forEach(chapterElement => {
    if (!chapterElement.classList.contains('display-none'))
      chapterElement.classList.add('display-none');

    var sectionElements = chapterElement.querySelectorAll('.section-content');
    sectionElements.forEach(sectionElement => {
      if (!sectionElement.classList.contains('display-none'))
        sectionElement.classList.add('display-none');
    });
  });
}

function fakeHighlight() {
  var chapterElement = chapterElements[fakePosition.chapter];
  var sectionElement = chapterElement.querySelectorAll('.section-content')[fakePosition.section - 1];
  var chapterTextElements = sectionElement.querySelectorAll('span[text-index]');
  var startTextElement = chapterTextElements[fakePosition.startIndex - 1];
  startTextElement.classList.add('highlight-start');
  var endTextElement = chapterTextElements[fakePosition.endIndex - 1];
  endTextElement.classList.add('highlight-end');
}

function getHeaderTwoOuterHeight() {
  const headerTwo = document.querySelector('h2');
  var height = headerTwo.clientHeight;
  var computedStyle = window.getComputedStyle(headerTwo);
  height += parseInt(computedStyle.marginTop);
  height += parseInt(computedStyle.marginBottom);
  return height;
}

function initAllSectionPageSum(direction) {
  // reset bookPageSum
  bookPageSum = { chapter: [] };

  chapterElements.forEach((chapterElement, chapterIndex) => {
    if (chapterElement.classList.contains('display-none'))
      chapterElement.classList.remove('display-none');

    var bookSectionsPageSum = { section: [] };

    sectionElements = chapterElement.querySelectorAll('.section-content');
    sectionElements.forEach((sectionElement, sectionIndex) => {
      if (sectionElement.classList.contains('display-none'))
        sectionElement.classList.remove('display-none');

      var pageSum = Math.ceil(sectionElement.scrollWidth / translatePosition[direction]);
      bookSectionsPageSum.section.push(pageSum);

      // 預設顯示 "引言" 章節中的第一部份，其餘 display: none
      if ((chapterIndex === 0 && sectionIndex !== 0) || chapterIndex !== 0)
        sectionElement.classList.add('display-none');
    });

    bookPageSum.chapter.push(bookSectionsPageSum);

    // 預設顯示 "引言" 章節，其餘 display: none
    if (chapterIndex !== 0)
      chapterElement.classList.add('display-none');
  });
}

function getCurrentSectionPageSum() {
  return bookPageSum.chapter[currentChapter].section[currentSection - 1];
}

function getCurrentSectionSum() {
  return bookPageSum.chapter[currentChapter].section.length;
}

function getChapterSum() {
  return bookPageSum.chapter.length;
}

function updateCurrentSectionOfChapterElements() {
  currentSectionOfChapterElements = currentChapterElement().querySelectorAll('.section-content');
}

function currentSectionElement() {
  return currentSectionOfChapterElements[currentSection - 1];
}

function currentChapterElement() {
  return chapterElements[currentChapter];
}

!(function ZoomInAndOutFontSize() {
  var fontSize = {
    levels: [12, 14, 16, 18, 20, 24, 30, 36, 42, 48, 52],
    levelIndex: 4,  // 預設字體大小 20px;
    zoomButtonElement: {
      in: document.querySelector('.zoom-in-font-size'),
      out: document.querySelector('.zoom-out-font-size')
    },
    getClassName: function () {
      var fontSizeLevel = fontSize.levels[fontSize.levelIndex];
      return `font-size-${fontSizeLevel}px`;
    }
  }

  fontSize.zoomButtonElement['in'].addEventListener('click', () => {
    if (fontSize.levelIndex < fontSize.levels.length - 1) {
      bookContentElement.classList.remove(fontSize.getClassName());
      fontSize.levelIndex++;
      bookContentElement.classList.add(fontSize.getClassName());
      initAllSectionPageSum(currentTextDirection);
    }
  });
  fontSize.zoomButtonElement['out'].addEventListener('click', () => {
    if (fontSize.levelIndex > 0) {
      bookContentElement.classList.remove(fontSize.getClassName());
      fontSize.levelIndex--;
      bookContentElement.classList.add(fontSize.getClassName());
      initAllSectionPageSum(currentTextDirection);
    }
  });
})();

!(function featureBar() {
  const featureBarSwitchElement = document.querySelector('.feature-bar-switch');

  featureBarSwitchElement.addEventListener('click', () => {
    featureBarElement.classList.toggle('display-none');
    backContainerElement.classList.toggle('display-none');
  });
})();

!(function displayAdjustment() {
  const element = {
    button: document.querySelector('.display-adjustment-btn'),
    container: document.querySelector('.display-adjustment__container')
  };
  // 點擊畫面調整按鈕開啟畫面調整區塊
  element['button'].addEventListener('click', () => {
    if (element['container'].classList.contains('display-none'))
      element['container'].classList.remove('display-none');
  });
  // 點擊畫面調整區塊之外的區域關閉畫面調整區塊
  element['container'].addEventListener('click', e => {
    if (e.target === element['container'] &&
        !element['container'].classList.contains('display-none'))
      element['container'].classList.add('display-none');
  });
})();

!(function share() {
  const shareButtonElement = document.querySelector('.share-btn');
  const shareContainerElement = document.querySelector('.share-container');
  const shareElement = document.querySelector('.share');

  // 點擊分享按鈕開啟分享區塊
  shareButtonElement.addEventListener('click', () => {
    if (shareContainerElement.classList.contains('display-none'))
      shareContainerElement.classList.remove('display-none');
  });
  // 點擊分享區塊之外區域關閉分享區塊
  shareContainerElement.addEventListener('click', () => {
    if (!shareContainerElement.classList.contains('display-none'))
      shareContainerElement.classList.add('display-none');
  });
  // 點擊分享區塊 (模擬分享書籍內文)
  shareElement.addEventListener('click', () => {
    if (!shareContainerElement.classList.contains('display-none')) {
      shareContainerElement.classList.add('display-none');
      console.log('share');
    }
  });
})();

// textDirectionSwitchButton = document.querySelector('.text-direction-switch');
// textDirectionSwitchButton.addEventListener('click', function () {
//   if (currentTextDirection === 'horizontal') {
//     removeNextPageOnClickEvent(currentTextDirection);
//     removePrevPageOnClickEvent(currentTextDirection);

//     currentTextDirection = 'vertical'
//     bookContentElement.className = 'book-content vertical-content';

//     createNextPageOnClickEvent(currentTextDirection);
//     createPrevPageOnClickEvent(currentTextDirection);
//   } else {
//     removeNextPageOnClickEvent(currentTextDirection);
//     removePrevPageOnClickEvent(currentTextDirection);

//     currentTextDirection = 'horizontal'
//     bookContentElement.className = 'book-content horizontal-content';

//     createNextPageOnClickEvent(currentTextDirection);
//     createPrevPageOnClickEvent(currentTextDirection);
//   }
// });

function createNextPageOnClickEvent(direction) {
  nextPageButtonElement.addEventListener('click', () => nextPage(direction));
}

function createPrevPageOnClickEvent(direction) {
  prevPageButtonElement.addEventListener('click', () => prevPage(direction));
}

function removeNextPageOnClickEvent(direction) {
  nextPageButtonElement.removeEventListener('click', () => nextPage(direction));
}

function removePrevPageOnClickEvent(direction) {
  prevPageButtonElement.removeEventListener('click', () => prevPage(direction));
}

function nextPage(direction) {
  console.log('------------------------');
  console.log(`prev log - chapter:${currentChapter}/${getChapterSum()}, section:${currentSection}/${getCurrentSectionSum()}, page:${currentSectionPage}/${getCurrentSectionPageSum()}`);
  if (isCurrentSectionNextPage()) {
    currentSectionPage++;
    console.log('same section, next page');
    console.log(`curt log - chapter:${currentChapter}/${getChapterSum()}, section:${currentSection}/${getCurrentSectionSum()}, page:${currentSectionPage}/${getCurrentSectionPageSum()}`);

  } else if (isNextSectionPage()) {
    hideCurrentSectionElement();
    currentSection++;
    currentSectionPage = 1;
    showCurrentSectionElement();
    console.log('next section, next page');
    console.log(`curt log - chapter:${currentChapter}/${getChapterSum()}, section:${currentSection}/${getCurrentSectionSum()}, page:${currentSectionPage}/${getCurrentSectionPageSum()}`);

  } else if (isNotLastChapterLastSectionPage() && isNextChapterPage()) {
    hideCurrentSectionElement();
    hideCurrentChapterElement();
    currentChapter++;
    currentSection = 1;
    currentSectionPage = 1;
    updateCurrentSectionOfChapterElements();
    showCurrentChapterElement();
    showCurrentSectionElement();
    console.log('next chapter, next page');
    console.log(`curt log - chapter:${currentChapter}/${getChapterSum()}, section:${currentSection}/${getCurrentSectionSum()}, page:${currentSectionPage}/${getCurrentSectionPageSum()}`);
  }

  if (isCurrentSectionNextPage() ||
      isNextSectionPage() ||
      isNextChapterPage())
    flipBook(direction);
}

function isCurrentSectionNextPage() {
  return currentSectionPage < getCurrentSectionPageSum() &&
      currentSection <= getCurrentSectionSum();
}

function isNextSectionPage() {
  return currentSectionPage === getCurrentSectionPageSum() &&
      currentSection < getCurrentSectionSum() &&
      currentChapter < getChapterSum();
}

function isNextChapterPage() {
  return currentSectionPage === getCurrentSectionPageSum() &&
      currentSection === getCurrentSectionSum() &&
      currentChapter < getChapterSum();
}

function isNotLastChapterLastSectionPage() {
  return currentChapter !== (getChapterSum() - 1);
}

function prevPage(direction) {
  console.log('------------------------');
  console.log(`prev log - chapter:${currentChapter}/${getChapterSum()}, section:${currentSection}/${getCurrentSectionSum()}, page:${currentSectionPage}/${getCurrentSectionPageSum()}`);
  if (isCurrentSectionPrevPage()) {
    currentSectionPage--;
    console.log('same section, prev page');
    console.log(`curt log - chapter:${currentChapter}/${getChapterSum()}, section:${currentSection}/${getCurrentSectionSum()}, page:${currentSectionPage}/${getCurrentSectionPageSum()}`);
    flipBook(direction);

  } else if (isPrevSectionPage()) {
    hideCurrentSectionElement();
    currentSection--;
    currentSectionPage = getCurrentSectionPageSum();
    showCurrentSectionElement();
    console.log('prev section, prev page');
    console.log(`curt log - chapter:${currentChapter}/${getChapterSum()}, section:${currentSection}/${getCurrentSectionSum()}, page:${currentSectionPage}/${getCurrentSectionPageSum()}`);
    flipBook(direction);

  } else if (isNotFirsrChapterFirstSectionPage() && isPrevChapterPage()) {
    hideCurrentSectionElement();
    hideCurrentChapterElement();
    currentChapter--;
    currentSection = getCurrentSectionSum();
    currentSectionPage = getCurrentSectionPageSum();
    updateCurrentSectionOfChapterElements();
    showCurrentChapterElement();
    showCurrentSectionElement();
    console.log('prev chapter, prev page');
    console.log(`curt log - chapter:${currentChapter}/${getChapterSum()}, section:${currentSection}/${getCurrentSectionSum()}, page:${currentSectionPage}/${getCurrentSectionPageSum()}`);
    flipBook(direction);
  }
}

function isCurrentSectionPrevPage() {
  return currentSectionPage > 1 &&
      currentSection >= 1;
}

function isPrevSectionPage() {
  return currentSectionPage === 1 && currentSection > 1 && currentChapter >= 0;
}

function isPrevChapterPage() {
  return currentSectionPage === 1 &&
      currentSection === 1 &&
      currentChapter > 0;
}

function isNotFirsrChapterFirstSectionPage() {
  return currentChapter !== 0;
}

function showCurrentSectionElement() {
  currentSectionElement().classList.remove('display-none');
}

function hideCurrentSectionElement() {
  currentSectionElement().classList.add('display-none');
}

function showCurrentChapterElement() {
  currentChapterElement().classList.remove('display-none');
}

function hideCurrentChapterElement() {
  currentChapterElement().classList.add('display-none');
}

function flipBook(direction) {
  var translate = {
    position: calcTranslatePosition(direction, currentSectionPage),
    direction: direction === 'horizontal' ? 'translateX' : 'translateY'
  }
  updateCurrentSectionOfChapterElements();
  currentSectionElement().style.transform = setCSSTranslate(translate);

  console.log(bookPageSum.chapter[currentChapter].section);
  console.log(`chapter:${currentChapter}/${getChapterSum()}, section:${currentSection}/${getCurrentSectionSum()}, page:${currentSectionPage}/${getCurrentSectionPageSum()}, ${direction}: ${setCSSTranslate(translate)}`);

  currentPageElement.innerText = 
      setCurrentPageText(currentSectionPage, getCurrentSectionPageSum());
}

function setCSSTranslate({direction, position}) {
  return `${direction}( -${position}px )`;
}

function calcTranslatePosition(direction, currentSectionPage) {
  return translatePosition[direction] * (currentSectionPage - 1);
}

function setCurrentPageText(currentPage, currentSectionPageSum) {
  return `本節第${currentPage}頁/共${currentSectionPageSum}頁`;
}


// book-content.html leave task

const leaveTipsShadow = document.querySelector('.leave-tips-shadow');
const backToTaskBtn = document.querySelector('.back-to-task');
const backToBookcase = document.querySelector('.back-to-bookcase');
const leaveTask = document.querySelector('.leave-task');
const checkLeave = document.querySelector('.check-leave');
const cancelLeave = document.querySelector('.cancel-leave');
const leaveTips = document.querySelector('.leave-tips');
const clearRecords = document.querySelector('.clear-records');
const clear = document.querySelector('.clear');
const cancelClear = document.querySelector('.cancel-clear');

// 顯示離開任務提示
backToBookcase.addEventListener('click', showLeaveTips);
function showLeaveTips(e) {
  e.stopPropagation();
  e.preventDefault();
  leaveTipsShadow.classList.add('show');
}

// 隱藏離開任務提示
backToTaskBtn.addEventListener('click', hideLeaveTips);
function hideLeaveTips(e) {
  e.stopPropagation();
  e.preventDefault();
  leaveTipsShadow.classList.remove('show');
}

// 回到任務、我要離開、離開、取消 按鈕顯示切換
leaveTask.addEventListener('click', showLeaveCheckBtn);
function showLeaveCheckBtn(e) {
  e.stopPropagation();
  e.preventDefault();
  checkLeave.classList.add('show');
  cancelLeave.classList.add('show');
  backToTaskBtn.classList.add('hide');
  leaveTask.classList.add('hide');
}

// 顯示清除記錄區塊
checkLeave.addEventListener('click', showClearRecords);
function showClearRecords(e) {
  e.stopPropagation();
  leaveTips.classList.add('hide');
  clearRecords.style.display = "flex";
}

// 隱藏清除記錄區塊
cancelClear.addEventListener('click', hideClearRecords);
function hideClearRecords(e) {
  e.stopPropagation();
  e.preventDefault();
  leaveTips.classList.remove('hide');
  clearRecords.style.display = "none";
}

// 跳轉至任務頁面
clear.addEventListener('click', backToTaskPage);
function backToTaskPage(e) {
  e.stopPropagation();
  e.preventDefault();
  window.location.replace('./threetask_test_complete.html');
}

// 回到任務、我要離開、離開、取消 按鈕顯示切換
cancelLeave.addEventListener('click', hideLeaveCheckBtn);
function hideLeaveCheckBtn(e) {
  e.stopPropagation();
  checkLeave.classList.remove('show');
  cancelLeave.classList.remove('show');
  backToTaskBtn.classList.remove('hide');
  leaveTask.classList.remove('hide');
}

// 筆記庫註記部分
const noteLibraryBtn = document.querySelector('.note-library-btn');
const backBtn = document.querySelector('.back');
const noteLibrary = document.querySelector('.note-library');
const noteLibraryShadow = document.querySelector('.note-library-shadow');
const note = document.querySelector('.note');

// show noteLibrary
noteLibraryBtn.addEventListener('click', showNoteLibrary);
function showNoteLibrary(e) {
  e.stopPropagation();
  noteLibraryShadow.classList.add('show');
  backToBookcase.classList.add('show');
  note.style.borderLeft = `15px solid ${fakePosition.color}`;
  note.style.marginLeft = '5px';
}

backBtn.addEventListener('click', hideNoteLibrary);
function hideNoteLibrary(e) {
  e.stopPropagation();
  noteLibrary.classList.remove('show');
  noteLibraryShadow.classList.remove('show');
}


// show tooltip
const dropDown = document.querySelector('.drop-down');
const tooltip = document.querySelector('.tooltip');

dropDown.addEventListener('click', showToolTip);
function showToolTip(e) {
  e.stopPropagation();
  tooltip.classList.add('show-tooltip');
}

// show editContainer
const editNote = document.querySelector('.edit-note');
const editContainer = document.querySelector('.edit-container');
const paragraph = document.querySelector('.paragraph');
const paragraphText = JSON.parse(localStorage.getItem('text')).highlightText;
let showText = document.querySelector('.show-text');
const save = document.querySelector('.save');
const cancel = document.querySelector('.cancel');
const textarea = document.querySelector('textarea');
const annotation = document.querySelector('.annotation');
annotation.textContent = '' || JSON.parse(localStorage.getItem('text')).comment;
textarea.textContent = '' || JSON.parse(localStorage.getItem('text')).comment;
showText.textContent = paragraphText;
paragraph.textContent =  '' || paragraphText;
editNote.addEventListener('click', showEditArea);

function showEditArea(e) {
  e.stopPropagation();
  editContainer.classList.add('show');
  showText.textContent = paragraphText;
}

// 儲存註解
save.addEventListener('click',saveText)
function saveText(e) {
  e.stopPropagation();
  editContainer.classList.remove('show');
  tooltip.classList.remove('show-tooltip');
  fakePosition.comment = textarea.value;
  localStorage.setItem('text', JSON.stringify(fakePosition));
  console.log(JSON.parse(localStorage.getItem('text')).comment);
  annotation.textContent = JSON.parse(localStorage.getItem('text')).comment;
}

// 取消編輯註解
cancel.addEventListener('click', cancelEdit)
function cancelEdit(e) {
  editContainer.classList.remove('show');
  textarea.value = JSON.parse(localStorage.getItem('text')).comment;
}

// 文章內容背景換色

const changeArticleColorWhite = document.querySelector('.button__white');
const changeArticleColorBlack = document.querySelector('.button__black');
const changeArticleColorPink = document.querySelector('.button__pink');
const changeArticleColorGreen = document.querySelector('.button__green');
const bookContent = document.querySelector('.book-content');

changeArticleColorWhite.addEventListener('click', changeBgColorWhite);
changeArticleColorBlack.addEventListener('click', changeBgBlack);
changeArticleColorPink.addEventListener('click', changeBgYellow);
changeArticleColorGreen.addEventListener('click', changeBgGreen);

function changeBgColorWhite(e) {
  bookContent.style.backgroundColor = "white";
  bookContent.style.color = "black";
  backContainerElement.style.color = 'black';
  backContainerElement.querySelector('img').style.filter = '';
}
function changeBgBlack(e) {
  bookContent.style.backgroundColor = "black";
  bookContent.style.color = "white";
  backContainerElement.style.color = 'white';
  backContainerElement.querySelector('img').style.filter = 'invert(1)';
}
function changeBgYellow(e) {
  bookContent.style.backgroundColor = "#fbf0da";
  bookContent.style.color = "black";
  backContainerElement.style.color = 'black';
  backContainerElement.querySelector('img').style.filter = '';
}
function changeBgGreen(e) {
  bookContent.style.backgroundColor = "#c5f8c8";
  bookContent.style.color = "black";
  backContainerElement.style.color = 'black';
  backContainerElement.querySelector('img').style.filter = '';
}

// tootip 刪除

const deleteBtn = document.querySelector('.delete');
deleteBtn.addEventListener('click', removeNote);

function removeNote(e) {
  console.log(e);
  var chapterElement = chapterElements[fakePosition.chapter];
  var sectionElement = chapterElement.querySelectorAll('.section-content')[fakePosition.section - 1];
  var chapterTextElements = sectionElement.querySelectorAll('span[text-index]');
  var startTextElement = chapterTextElements[fakePosition.startIndex - 1];
  startTextElement.classList.remove('highlight-start');
  var endTextElement = chapterTextElements[fakePosition.endIndex - 1];
  endTextElement.classList.remove('highlight-end');
  noteLibrary.removeChild(note);
  tooltip.classList.remove('show-tooltip');
}