const edit = document.querySelector('.edit');
const paragraph = document.querySelector('.paragraph');
let showText = document.querySelector('.show-text');
const editContainer = document.querySelector('.edit-container');
const paragraphText = paragraph.textContent;
const textarea = document.querySelector('textarea');
const note = document.querySelector('.note');
const tooltip = document.querySelector('.tooltip');
const dropDown = document.querySelector('.drop-down');
const save = document.querySelector('.save');
const cancel = document.querySelector('.cancel');

// show Tooltip
dropDown.addEventListener('click', showTooltip);
function showTooltip(e) {
  e.stopPropagation();
  tooltip.classList.add('show');
}

// show editContainer
edit.addEventListener('click', showEditArea)
function showEditArea(e) {
  e.stopPropagation();
  editContainer.classList.add('show');
  showText.textContent = paragraph.textContent;
}

// 儲存註解
save.addEventListener('click',saveText)
function saveText(e) {
  e.stopPropagation();
  editContainer.classList.remove('show');
  tooltip.classList.remove('show');
  const newText = textarea.value;
  localStorage.setItem('text', JSON.stringify(newText));
  note.textContent = JSON.parse(localStorage.getItem('text'));
}

// 取消編輯註解
cancel.addEventListener('click', cancelEdit)
function cancelEdit(e) {
  editContainer.classList.remove('show');
  textarea.value = JSON.parse(localStorage.getItem('text'));
}

