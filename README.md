# Web Camp 2019 壓力鍋：實作電子書看看？
###### tags: `好想工作室`, `web camp`, `壓力鍋`, `接案練習`

## 壓力鍋及專案說明

## 時程，全程使用 ZOOM
- 2019/11/04 13:30 (一) 與業主的第一次親蜜接觸 meeting
- 2019/11/06 10:00 (三) 有問題問業主 meeting
- 2019/11/08 17:00 (五) 業主的初步看結果 meeting，討論優勝者獲得什麼
- 2019/11/11 17:00 (一) 業主驗收 meeting

## Scope
1. 切版：直書，橫書
2. JavaScript：DOM、event 操作
3. JSON：資料整理

## 分支命名規則
> 年月_web_camp_pressure_game_隊名
> (舉例：201911_web_camp_pressure_game_Lorem）

## 任務
- [ ] 選擇任務 => 直書/橫書（每個功能區塊有直與橫的排版差異）
- [ ] 電子書直橫排版都要能翻頁
- [ ] 畫重點
- [ ] 針對重點換色
- [ ] 針對重點寫筆記
- [ ] 記錄完成任務的時間與點擊次數
- [ ] 任務列表分為「完成」與「未完成」的狀態，點擊完成的任務要能看見紀錄

## 素材
### 設計稿
#### 靜態：[zeplin](https://zpl.io/br8MkY5)，邀請制，須提供你的信箱在這個 [HackMD](https://hackmd.io/tjYBue6iTMGrgrnEUBTL2Q)，並透過發 `Pull Request` 的方式，告訴業主你們組別已經 clone repo 了，業主會邀請你的信箱加入設計稿的 workspace，到時請再收信確認即可。

#### mockup：[墨刀-任務一](https://mockingbot.com/app/dec991a078eb56dba1f1f72aa4c31ab745b5574a#screen=s92ea89d09c155150779000)、[墨刀-任務二](https://mockingbot.com/app/0c39fb7bc9dff2958f4412136ec3fe18ce230bb6#screen=sb657f116af155422095500)

### 書籍文稿
#### 已在專案當中：`book.txt`，請自行整理。

- 註：整個專案不可作為商用，素材請勿外流，謝謝大家的配合！

## Demo
- PX-T：https://titangene.gitlab.io/e-book/